# Copyright 2009 Sterling X. Winter <replica@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require java [ allow_native=true has_javadocs=false ]

SUMMARY="A Javadoc-compatible documentation generation framework for Java source files"
DESCRIPTION="
Gjdoc provides a framework for generating documentation in various formats from
Java source files. It implements all features of the traditional javadoc tool
version 1.4 (except for one unimplemented option, -serialwarn), is fully
command-line compatible, and provides a compatible Doclet API. In contrast to
javadoc, gjdoc does not perform syntax checking on the supplied sources. If
necessary, use 'gcj -fsyntax-only' for checking the sources beforehand. This
version has no javadoc 1.5 functionality, meaning generics, annotations and
static imports are not supported.
"
HOMEPAGE="http://www.gnu.org/software/cp-tools/"
DOWNLOADS="mirror://gnu/classpath/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="xmldoclet"

DEPENDENCIES="
    build+run:
        dev-java/antlr[>=2.7.1][java]
"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( native xmldoclet )

