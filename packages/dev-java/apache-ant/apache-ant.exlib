# Copyright 2009-2018 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2009 Sterling X. Winter <replica@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require java

export_exlib_phases src_unpack src_prepare src_compile src_test src_install

MY_PN="${PN/apache-}"

SUMMARY="Java-based build tool similar to 'make' that uses XML configuration files"
DESCRIPTION="
Ant is a Java based build tool, similar to make, but with better support for the
cross platform issues involved with developing Java applications. Ant is the
build tool of choice for all Java projects at Apache and many other Open Source
Java projects.
"
HOMEPAGE="https://${MY_PN}.apache.org/"
DOWNLOADS="
    https://apache.org/dist/ant/source/${PNV}-src.tar.bz2
    https://archive.apache.org/dist/ant/source/${PNV}-src.tar.bz2
    https://search.maven.org/remotecontent?filepath=org/hamcrest/hamcrest-core/1.3/hamcrest-core-1.3.jar
    https://search.maven.org/remotecontent?filepath=org/hamcrest/hamcrest-library/1.3/hamcrest-library-1.3.jar
    https://search.maven.org/remotecontent?filepath=junit/junit/3.8.2/junit-3.8.2.jar
    https://search.maven.org/remotecontent?filepath=junit/junit/4.12/junit-4.12.jar
"
#    mirror://apache/${MY_PN}/source/${PNV}-src.tar.bz2

BUGS_TO="philantrop@exherbo.org"

UPSTREAM_DOCUMENTATION="https://ant.apache.org/manual/index.html [[ lang = en ]]"

LICENCES="Apache-2.0"
SLOT="0"
MYOPTIONS="
    commons-logging [[ description = [ Include CommonsLoggingListener ] ]]
    debug
    log4j [[ description = [ Include Log4jListener ] ]]

    ant_data_types: classfileset regexp xmlcatalog
    ant_scripting:  beanshell groovy javascript netrexx python ruby tcl
    ant_tasks:      antlr ftp image jdepend junit junitreport mail rexec scp
                    script sshexec telnet
"

# dev-java/rhino-1.5_p4 is rhino-1.5_r4 upstream
DEPENDENCIES="
    build+run:
        commons-logging? ( dev-java/jakarta-commons-logging )
        log4j? ( dev-java/log4j )

        ant_data_types:classfileset? ( dev-java/bcel )
        ant_data_types:regexp? ( dev-java/jakarta-oro[>=2.0.8] )
        ant_data_types:xmlcatalog? ( dev-java/xml-commons )

        ant_scripting:beanshell? ( dev-java/beanshell[>=1.3] )
        ant_scripting:groovy? (
            dev-java/antlr
            dev-lang/groovy
            dev-java/jakarta-oro[>=2.0.8]
        )
        ant_scripting:javascript? ( dev-java/rhino[>=1.5_p4] )
        ant_scripting:netrexx? ( dev-lang/netrexx )
        ant_scripting:python? ( dev-lang/jython )
        ant_scripting:ruby? ( dev-lang/jruby )
        ant_scripting:tcl? ( dev-lang/tcl )

        ant_tasks:antlr? ( dev-java/antlr )
        ant_tasks:ftp? (
            dev-java/jakarta-commons-net[>=1.4.0]
            dev-java/jakarta-oro[>=2.0.8]
        )
        ant_tasks:image? ( java-media/jai )
        ant_tasks:jdepend? ( dev-java/jdepend )
        ant_tasks:junit? ( dev-java/junit )
        ant_tasks:junitreport? ( dev-java/xalan-java )
        ant_tasks:mail? (
            dev-java/jaf
            dev-java/javamail
        )
        ant_tasks:rexec? (
            dev-java/jakarta-commons-net[>=1.4.0]
            dev-java/jakarta-oro[>=2.0.8]
        )
        ant_tasks:scp? ( dev-java/jsch )
        ant_tasks:script? (
            dev-java/bsf[>=2.4.0]
            dev-java/jakarta-commons-logging
        )
        ant_tasks:sshexec? ( dev-java/jsch )
        ant_tasks:telnet? (
            dev-java/jakarta-commons-net[>=1.4.0]
            dev-java/jakarta-oro[>=2.0.8]
        )
"

DEFAULT_SRC_PREPARE_PATCHES=( -p1 "${FILES}"/${PN}-1.8.3_java-home.patch )

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( "WHATSNEW" )

_change_build_xml() {
    edo sed \
        -e "s:\(.*<property name=\"$1\"\).* \(value=\).*\(/>\)$:\1 \2\"$2\"\3:" \
        -i "${WORK}"/build.xml
}

_build_sh() {
    edo "${WORK}"/build.sh \
        -Ddist.dir="${IMAGE}/usr/share/ant" "$@"
}

apache-ant_src_unpack() {
    unpack --if-compressed ${ARCHIVES//hamcrest-core-1.3.jar}
    unpack --if-compressed ${ARCHIVES//hamcrest-library-1.3.jar}
    unpack --if-compressed ${ARCHIVES//junit-3.8.2.jar}
    unpack --if-compressed ${ARCHIVES//junit-4.12.jar}
}

apache-ant_src_prepare() {
    # Kill those evil bundled libs...
    edo rm -r "${WORK}"/lib/*
    # ... and add an evil bundled lib ourselves to make the tests happy.
    edo mkdir "${WORK}"/lib/optional
    edo cp "${FETCHEDDIR}"/hamcrest-core-1.3.jar "${WORK}"/lib/optional
    edo cp "${FETCHEDDIR}"/hamcrest-library-1.3.jar "${WORK}"/lib/optional
    edo cp "${FETCHEDDIR}"/junit-3.8.2.jar "${WORK}"/lib/optional
    edo cp "${FETCHEDDIR}"/junit-4.12.jar "${WORK}"/lib/optional

    edo sed \
        -e 's:\(depends="jars\),test-jar":\1":' \
        -i build.xml

    option debug || _change_build_xml debug false

    default
}

apache-ant_src_compile() {
    _build_sh jars
}

apache-ant_src_test() {
    _build_sh test-jar
}

apache-ant_src_install() {
    _build_sh dist-lite

    # Remove cruft
    edo find "${IMAGE}"/usr/share/ant -type f -iname "*.bat" -o -iname "*.cmd" -delete

    hereenvd 50ant <<EOF
ANT_HOME=/usr/share/ant
EOF

    default

    insinto /usr/$(exhost --target)/bin
    for f in ant antRun antRun.pl complete-ant-cmd.pl runant.pl runant.py ; do
        dosym /usr/share/ant/bin/${f} /usr/$(exhost --target)/bin/${f}
    done
}

